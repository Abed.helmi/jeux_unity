﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Moveorb : MonoBehaviour
{
    public KeyCode moveL;
    public KeyCode moveR;
    public float horizVel = 0;
    public int num = 2;
    //private string controlLocked = "n";
 
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(horizVel, GM.vertVel, 4);

        if (Input.GetKeyDown(moveL)&&(num>1))//&&( controlLocked == "n"))
        {
            horizVel = -2;
            StartCoroutine (stopSlide ());
            num -= 1;
            //controlLocked = "y";
}

        if (Input.GetKeyDown(moveR) && (num<3))//&(controlLocked == "n"))
        {
            horizVel = 2;
            StartCoroutine(stopSlide());
            num += 1;
    //controlLocked = "y";

        }

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "lethal")
        {
            Destroy (gameObject);
            GM.y = "Fail";
        }
        if (other.gameObject.name == "Capsule")
        {
            Destroy (other.gameObject);
        }

    }
  void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "rampbottmtrig")
        {
            GM.vertVel = 2;
        }
        if (other.gameObject.name == "ramptoptrig")
        {
            GM.vertVel = 0;
        }
        if (other.gameObject.name == "rampbottmtrig1")
        {
            GM.vertVel = 1;
        }
        if (other.gameObject.name == "ramptoptrig2")
        {
            GM.vertVel =0;
        }

        if (other.gameObject.name == "exit")
        {
            GM.y = "Sucess";
            SceneManager.LoadScene("LevelComp");
        }
        if (other.gameObject.name == "coin")
        {
            Destroy(other.gameObject);
            GM.CoinTotal += 1;
        }


    }
    IEnumerator stopSlide()
    {
        yield return new WaitForSeconds(.5f);
        horizVel = 0;
     //controlLocked = "n";
}
}